#!/bin/bash

# Sapporo DC - Virtual Machine Template 
source ./env.txt
govc_clone(){
govc vm.clone -host=$4 -ds=datastore12 -vm $2 $1
govc vm.power -off $1
govc vm.customize -vm $1 -ip $3 -netmask 255.255.255.0 -gateway 172.16.11.254 -dns-server 172.16.11.21
govc vm.power -on $1
govc vm.change -vm $1 -sync-time-with-host
}

custom_vm(){
govc vm.customize -vm $1 -ip $3 -netmask 255.255.255.0 -gateway 172.16.11.254 -dns-server 172.16.11.21
}
create_vm(){
govc_clone poc-vm01 linux-template 172.16.11.87 172.16.11.43
#custom_vm poc-vm01 linux-template 172.16.11.87 172.16.11.41
}

create_vm
