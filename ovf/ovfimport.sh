#!/bin/bash 
source ./env.txt

RZ=$(govc find vm -name linux-template)

# check whether linux-template exists in datacenter
if [ ! -z $RZ ];then
 govc vm.destroy linux-template
fi

RZ=$(govc find vm -name poc-vm01)

# check whether poc-vm01 exists in datacenter
if [ ! -z $RZ ];then
 govc vm.destroy poc-vm01
fi

RZ=$(govc find vm -name linux-template)

# upload the ovf template and create virutal machine template in datacenter
if [  -z $RZ ];then
 govc import.ovf -host=172.16.11.43 -ds=datastore12 ./linux-template/linux-template.ovf
 govc vm.markastemplate linux-template
fi
