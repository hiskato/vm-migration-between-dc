# VM Migration POC

This repository contains sample intersight cloud orchestrator workflow.
DISCLAIMER: This is NOT an official Cisco repository and comes with NO WARRANTY AND/OR SUPPORT
Please check LICENSE-CISCO for additional details

![Intersight workflow](images/wk.png)

## STEP for the Setup
1. The Workflow which is provided in this repository needed to be imported to Intersight. - Refer to
[workflow import](https://intersight.com/help/saas/resources/Workflow_Designer)
2. Preparing the Linux host to invoke the script.
3. SSH Target registration for the Linux host is required in Intersight.
4. Place the scripts in your home directory. This example shows under /home/icoadm/ovf. 
5. [govc](https://github.com/vmware/govmomi/tree/main/govc) needs to be installed in the linux host.


